import React from "react";
import { cookies } from "next/headers";
import { ButtonAdvice, ButtonLogin, DropdownMenuProfile, Navigation } from "@/components";
import { Separator } from "@/components/ui/separator";
import { MSB_AUTH_COOKIE } from "@/lib/utils";
import PhoneIcon from "@/components/icons/Phone";
import { Button } from "@/components/ui/button";

const Navbar = () => {
	const nextCookies = cookies();

	const userServer = nextCookies.get(MSB_AUTH_COOKIE)?.value;

	return (
		<>
			<ul className={"flex items-center justify-center gap-4"}>
				<div className={"hidden lg:flex items-center justify-center gap-4"}>
					<Navigation />
					{!userServer && <ButtonLogin />}
					<ButtonAdvice />
					<a href={"tel:1900 6083"}>
						<Button variant={"ghost"}>
							<PhoneIcon className={"mr-1"} />
							<span>1900 6083</span>
						</Button>
					</a>
				</div>
				{!!userServer && (
					<>
						<Separator orientation={"vertical"} className={"hidden lg:block h-4 w-[1.5px]"} />
						<DropdownMenuProfile user={JSON.parse(userServer)} />
					</>
				)}
			</ul>
		</>
	);
};

export default Navbar;
