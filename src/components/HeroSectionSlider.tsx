"use client";
import React from "react";
import { HeroSection, SwiperSliderPagination } from "@/components";

const Heros = [
	{
		id: 1,
		content: <HeroSection />,
	},
	{
		id: 2,
		content: <HeroSection />,
	},
];
const HeroSectionSlider = () => {
	return (
		<div className={"bg-white"}>
			<SwiperSliderPagination duration={5000} data={Heros} renderItem={(item) => <>{item?.content}</>} />
		</div>
	);
};

export default HeroSectionSlider;
