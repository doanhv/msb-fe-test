//@ts-ignore
"use client";
import React, { Fragment } from "react";
import { Sheet, SheetContent, SheetHeader, SheetTitle, SheetTrigger } from "@/components/ui/sheet";
import { Button } from "@/components/ui/button";
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import { Checkbox } from "@/components/ui/checkbox";
import { cn } from "@/lib/utils";
import { Check, ChevronsUpDown } from "lucide-react";
import { Command, CommandEmpty, CommandGroup, CommandInput, CommandItem } from "@/components/ui/command";
import { Popover, PopoverContent, PopoverTrigger } from "@/components/ui/popover";
import { FormItemType, getFormAskForAdvices } from "@/lib/getFormAskForAdvices";
import { ComponentType } from "@/interfaces";
import { productAdvices } from "@/lib/getProductAdvices";
import { getProvinces } from "@/lib/getProvinces";
import { getDistricts } from "@/lib/getDistricts";

const formSchema = z.object({
	fullName: z
		.string()
		.min(1, { message: "Vui lòng nhập họ tên." })
		.max(100, { message: "Họ tên quá dài. Vui lòng nhập ít hơn 100 ký tự." })
		.regex(/^[a-zA-Z\s']+$/, { message: "Họ tên chỉ được chứa chữ cái, khoảng trắng và dấu nháy đơn." })
		.refine((value) => !value.includes("  "), { message: "Không được nhập hai dấu cách liền nhau." })
		.refine((value) => value.trim() === value, { message: "Không được nhập dấu cách ở đầu hoặc cuối chuỗi." })
		.refine((value) => value.includes(" "), {
			message: "Vui lòng nhập họ và tên đầy đủ, có dấu cách ở giữa và chỉ chứa dấu nháy đơn.",
		}),
	phone: z
		.string()
		.optional()
		.refine((value) => /^(0|\+84)([0-9]{9,10})$/.test(value), { message: "Số điện thoại không hợp lệ." }),
	province: z.string().optional(),
	district: z.string().or(z.number()).optional(),
	gender: z.enum(["male", "female"]),
	productAdvices: z.array(z.string()),
	description: z.string().optional(),
});

const ButtonAdvice = () => {
	const form = useForm<z.infer<typeof formSchema>>({
		resolver: zodResolver(formSchema),
		defaultValues: {
			fullName: "",
			phone: "",
			gender: "male",
			productAdvices: ["credit", "accountPro"],
		},
	});

	const onSubmit = (values: z.infer<typeof formSchema>) => {
		console.log(values);
	};

	const renderFormItem = (item: FormItemType) => {
		const options = {
			province: getProvinces,
			district: getDistricts,
		};
		const renderer = {
			[ComponentType.input]: (
				<FormField
					control={form.control}
					name={item.name}
					render={({ field }) => (
						<FormItem>
							<FormLabel>{item.label}</FormLabel>
							<FormControl>
								<Input placeholder={item.placeholder} {...field} />
							</FormControl>
							<FormMessage />
						</FormItem>
					)}
				/>
			),
			[ComponentType.radio]: (
				<FormField
					control={form.control}
					name={item.name}
					render={({ field }) => (
						<FormItem>
							<FormLabel className={"mb-4"}>Giới tính</FormLabel>
							<FormControl>
								<RadioGroup onValueChange={field.onChange} defaultValue={field.value} className={"flex gap-14"}>
									<div className="flex items-center space-x-2">
										<RadioGroupItem value="male" id="male" />
										<Label htmlFor="male">Nam</Label>
									</div>
									<div className="flex items-center space-x-2">
										<RadioGroupItem value="female" id="female" />
										<Label htmlFor="female">Nữ</Label>
									</div>
								</RadioGroup>
							</FormControl>
							<FormMessage />
						</FormItem>
					)}
				/>
			),
			[ComponentType.textarea]: (
				<FormField
					control={form.control}
					name={item.name}
					render={({ field }) => (
						<FormItem>
							<FormLabel>{item.label}</FormLabel>
							<FormControl>
								<Textarea placeholder={item.placeholder} {...field} />
							</FormControl>
							<FormMessage />
						</FormItem>
					)}
				/>
			),
			[ComponentType.checkbox]: (
				<FormItem>
					<div className="my-4">
						<FormLabel>{item.label}</FormLabel>
					</div>
					<div className={"grid grid-cols-2 gap-4"}>
						{productAdvices.map((product) => (
							<FormField
								key={product.id}
								control={form.control}
								name={item.name}
								render={({ field }) => {
									return (
										<FormItem key={product.id} className="flex flex-row items-start space-x-3 space-y-0">
											<FormControl>
												<Checkbox
													checked={field.value?.includes(product.id)}
													onCheckedChange={(checked) => {
														return checked
															? field.onChange([...field.value, product.id])
															: field.onChange(field.value?.filter((value) => value !== product.id));
													}}
												/>
											</FormControl>
											<FormLabel className="font-normal">{product.label}</FormLabel>
										</FormItem>
									);
								}}
							/>
						))}
					</div>
					<FormMessage />
				</FormItem>
			),
			[ComponentType.select]: (
				<FormField
					control={form.control}
					name={item.name}
					render={({ field }) => (
						<FormItem>
							<FormLabel>{item.label}</FormLabel>
							<Popover>
								<PopoverTrigger asChild>
									<FormControl>
										<Button
											variant="outline"
											role="combobox"
											className={cn(
												"w-full justify-between border-input hover:text-muted-foreground",
												!field.value && "text-muted-foreground",
											)}
										>
											{field.value
												? options[item.name].find((option) => option.value === field.value)?.label
												: item.placeholder}
											<ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
										</Button>
									</FormControl>
								</PopoverTrigger>
								<PopoverContent className="w-[200px] p-0">
									<Command>
										<CommandInput placeholder={item.placeholder} />
										<CommandEmpty>Không tìm thấy.</CommandEmpty>
										<CommandGroup>
											{options[item.name].map((option) => (
												<CommandItem
													value={option.label}
													key={option.value}
													onSelect={() => {
														form.setValue(item.name, option.value);
													}}
												>
													<Check
														className={cn("mr-2 h-4 w-4", option.value === field.value ? "opacity-100" : "opacity-0")}
													/>
													{option.label}
												</CommandItem>
											))}
										</CommandGroup>
									</Command>
								</PopoverContent>
							</Popover>
						</FormItem>
					)}
				/>
			),
		};
		return <div key={item.name}>{renderer[item.type]}</div>;
	};

	return (
		<>
			<Sheet>
				<SheetTrigger asChild>
					<Button variant={"outline"}>Yêu cầu tư vấn</Button>
				</SheetTrigger>
				<SheetContent className={"p-0 h-screen flex flex-col gap-0"}>
					<SheetHeader className={"p-4 border-b border-solid border-neutral-200"}>
						<SheetTitle>Yêu cầu tư vấn</SheetTitle>
					</SheetHeader>
					<div className={"p-4 flex-grow overflow-y-auto"}>
						<Form {...form}>
							<form onSubmit={form.handleSubmit(onSubmit)} className={"h-full flex flex-col"}>
								<p className={"title mb-4 font-medium"}>Thông tin khách hàng</p>
								<div className="space-y-4 flex-grow">
									{getFormAskForAdvices.map((formItem, index) => (
										// biome-ignore lint/suspicious/noArrayIndexKey: <explanation>
										<Fragment key={index}>
											<div className={cn("grid gap-4", `grid-cols-${formItem.length}`)}>
												{formItem.map((item) => renderFormItem(item))}
											</div>
										</Fragment>
									))}
								</div>
								<Button type="submit">Submit</Button>
							</form>
						</Form>
					</div>
				</SheetContent>
			</Sheet>
		</>
	);
};

export default ButtonAdvice;
