import React, { ReactNode } from "react";
import { Card, CardContent, CardHeader } from "@/components/ui/card";

type CardDashboardProps = {
	title: string;
	children?: ReactNode;
};
const CardDashboard = ({ title, children }: CardDashboardProps) => {
	return (
		<Card className={"border border-solid border-neutral-200 overflow-hidden"}>
			<CardHeader className={"title p-4 border-b border-solid border-neutral-200"}>{title}</CardHeader>
			<CardContent className={"p-4 min-h-[124px]"}>{children}</CardContent>
		</Card>
	);
};

export default CardDashboard;
