"use client";

import React from "react";
import {
	DropdownMenu,
	DropdownMenuContent,
	DropdownMenuItem,
	DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import Link from "next/link";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { UserType } from "@/interfaces";
import { DropdownMenuProfileSkeleton, FullName } from "@/components";
import { logout } from "@/lib/auth";
import { getFullName } from "@/lib/utils";

const DropdownMenuProfile = ({ user }: { user: UserType }) => {
	const router = useRouter();
	const signoutMutation = useMutation({
		mutationFn: logout,
	});

	if (!user) return <DropdownMenuProfileSkeleton />;

	const onLogout = () => {
		signoutMutation.mutate(undefined, {
			onSuccess: () => {
				router.refresh();
			},
		});
	};

	return (
		<DropdownMenu>
			<DropdownMenuTrigger asChild>
				<div className={"flex items-center justify-center gap-2 cursor-pointer"}>
					<Avatar className={"w-8 h-8"}>
						<AvatarImage src={user?.image} />
						<AvatarFallback>{getFullName(user?.firstName, user?.lastName).slice(0, 1)}</AvatarFallback>
					</Avatar>
					<p className={"font-medium"}>
						<FullName user={user} />
					</p>
				</div>
			</DropdownMenuTrigger>
			<DropdownMenuContent className={"shadow-none"} sideOffset={24}>
				<Link href={"/dashboard/account"} passHref>
					<DropdownMenuItem>Quản lý tài khoản</DropdownMenuItem>
				</Link>
				<DropdownMenuItem onSelect={onLogout}>Đăng xuất</DropdownMenuItem>
			</DropdownMenuContent>
		</DropdownMenu>
	);
};

export default DropdownMenuProfile;
