"use client";
import React, { useMemo } from "react";
import { UserType } from "@/interfaces";
import { getFullName } from "@/lib/utils";

const FullName = ({ user }: { user?: UserType }) => {
	const fullName = useMemo(() => getFullName(user?.firstName, user?.lastName), [user]);
	return <span>{fullName}</span>;
};

export default FullName;
