"use client";
import React from "react";
import {
	DropdownMenuItem,
	DropdownMenuPortal,
	DropdownMenuSub,
	DropdownMenuSubContent,
	DropdownMenuSubTrigger,
} from "@/components/ui/dropdown-menu";
import { MenuType } from "@/interfaces";
import Link from "next/link";

type DropdownMenuSubCustomProps = {
	menu: MenuType;
	onCloseMenu?: () => void;
};
const DropdownMenuSubCustom = ({ menu, onCloseMenu }: DropdownMenuSubCustomProps) => {
	return (
		<DropdownMenuSub>
			<DropdownMenuSubTrigger>{menu.label}</DropdownMenuSubTrigger>
			<DropdownMenuPortal>
				<DropdownMenuSubContent sideOffset={2} className={"p-0"}>
					{menu.children?.map((sub) => (
						<DropdownMenuItem onSelect={onCloseMenu} key={sub.id} asChild>
							<Link href={sub.path as string}>{sub.label}</Link>
						</DropdownMenuItem>
					))}
				</DropdownMenuSubContent>
			</DropdownMenuPortal>
		</DropdownMenuSub>
	);
};

export default DropdownMenuSubCustom;
