"use client";

import React from "react";
import { ProductCard, ProductList } from "@/components";
import { useSuspenseQuery } from "@tanstack/react-query";
import { ProductType } from "@/interfaces";
import { getProducts } from "@/lib/getProducts";

const ProductSection = () => {
	const { data } = useSuspenseQuery<ProductType[]>({
		queryKey: ["products"],
		queryFn: getProducts,
	});

	return (
		<section className={"container mx-auto pb-[90px]"}>
			<h4 className={"text-center mb-10"}>Danh sách sản phẩm</h4>
			<ProductList data={data} renderItem={(item) => <ProductCard key={item.id} {...item} />} />
		</section>
	);
};

export default ProductSection;
