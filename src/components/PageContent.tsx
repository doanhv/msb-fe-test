import React, { Suspense } from "react";
import { FeatureSection, HeroSectionSlider, ProductSection, ProductSectionSkeleton } from "@/components";

const PageContent = () => {
	return (
		<main className="min-h-screen">
			<HeroSectionSlider />

			{/*TODO: */}
			<FeatureSection />
			<Suspense fallback={<ProductSectionSkeleton />}>
				<ProductSection />
			</Suspense>
		</main>
	);
};

export default PageContent;
