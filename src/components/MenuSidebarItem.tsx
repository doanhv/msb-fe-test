"use client";
import React from "react";
import { MenuType } from "@/interfaces";
import { cn } from "@/lib/utils";
import { usePathname, useRouter } from "next/navigation";
import Link from "next/link";
import { useMutation } from "@tanstack/react-query";
import { logout } from "@/lib/auth";

const MenuSidebarItemLogout = ({ icon, label }: Partial<MenuType>) => {
	const router = useRouter();

	const signoutMutation = useMutation({
		mutationFn: logout,
	});
	const onLogout = async () => {
		signoutMutation.mutate(undefined, {
			onSuccess: () => {
				router.refresh();
			},
		});
	};

	return (
		// biome-ignore lint/a11y/useKeyWithClickEvents: <explanation>
		<li
			className={cn("inline-flex w-full cursor-pointer font-medium gap-3 p-4 no-underline outline-none")}
			onClick={onLogout}
		>
			{icon}
			{label}
		</li>
	);
};
const MenuSidebarItem = ({ icon, label, path }: MenuType) => {
	const pathname = usePathname();

	const isActiveLink = pathname.includes(path as string);

	if (path) {
		return (
			<Link href={path as string} legacyBehavior passHref>
				<li
					className={cn(
						"inline-flex w-full cursor-pointer font-medium gap-3 p-4 no-underline outline-none",
						isActiveLink && "bg-primary-25 text-primary-500 hover:bg-primary-50 hover:text-primary-600",
					)}
				>
					<span className={"w-6"}>{icon}</span>
					{label}
				</li>
			</Link>
		);
	}
	return <MenuSidebarItemLogout label={label} icon={icon} />;
};

export default MenuSidebarItem;
