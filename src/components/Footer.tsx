import React from "react";
import Link from "next/link";

const Footer = () => {
	return (
		<footer className={"container mx-auto py-6"}>
			<div className={"flex flex-col lg:flex-row items-center justify-between gap-4 text-neutral-foreground"}>
				<p>Bản quyền © 2021 thuộc về Ngân hàng TMCP Hàng Hải Việt Nam MSB</p>
				<div className={"flex items-center justify-center gap-4"}>
					<Link href={"/"}>
						<p>Điều khoản dịch vụ</p>
					</Link>
					<Link href={"/"}>
						<p>Ngân hàng điện tử</p>
					</Link>
				</div>
			</div>
		</footer>
	);
};

export default Footer;
