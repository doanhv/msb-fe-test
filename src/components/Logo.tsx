import React from "react";
import Image from "next/image";
import Link from "next/link";

const Logo = () => {
	return (
		<>
			<Link href={"/"}>
				<Image src={"/logo-ngang.svg"} alt={"Logo MSB"} width={100} height={24} className={"min-w-[100px]"} />
			</Link>
		</>
	);
};

export default Logo;
