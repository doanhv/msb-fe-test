import React, { ReactNode } from "react";

type ProductListProps<T> = {
	data: T[];
	renderItem: (item: T) => ReactNode;
};
const ProductList = <T extends {}>({ data, renderItem }: ProductListProps<T>) => {
	return (
		<div className={"grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-x-4 gap-y-10"}>{data?.map(renderItem)}</div>
	);
};

export default ProductList;
