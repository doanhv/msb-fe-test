import React from "react";
import { Logo, Navbar } from "@/components";

const Header = () => {
	return (
		<header className={"flex items-center justify-between h-18 px-10 py-4 bg-white sticky z-50 top-0 left-0"}>
			<Logo />
			<Navbar />
		</header>
	);
};

export default Header;
