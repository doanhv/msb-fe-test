import React from "react";
import { Skeleton } from "@/components/ui/skeleton";

const ProductSectionSkeleton = () => {
	return (
		<section className={"container mx-auto"}>
			<h4 className={"text-center mb-10"}>Danh sách sản phẩm</h4>
			<div className={"grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-x-4 gap-y-10"}>
				<Skeleton className={"h-[394px]"} />
				<Skeleton className={"h-[394px]"} />
				<Skeleton className={"h-[394px]"} />
				<Skeleton className={"h-[394px]"} />
				<Skeleton className={"h-[394px]"} />
				<Skeleton className={"h-[394px]"} />
			</div>
		</section>
	);
};

export default ProductSectionSkeleton;
