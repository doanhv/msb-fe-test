"use client";

import React, { ReactNode } from "react";

import { Autoplay, Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import { SwiperOptions } from "swiper/types";

type SliderPaginationProps<T> = {
	data?: T[];
	renderItem?: (item: T, index: number) => ReactNode;
	duration?: number;
} & SwiperOptions;

const SwiperSliderPagination = <T extends {}>({
	data = [],
	renderItem = () => null,
	duration = 5000,
	...props
}: SliderPaginationProps<T>) => {
	return (
		<>
			<Swiper
				spaceBetween={30}
				slidesPerView={1}
				centeredSlides
				autoplay={{
					delay: duration,
					disableOnInteraction: false,
				}}
				loop
				noSwiping
				noSwipingClass={'swiper-slide'}
				pagination={{
					clickable: true,
				}}
				modules={[Autoplay, Pagination]}
				{...props}
			>
				{data?.map((item, index) => (
					// biome-ignore lint/suspicious/noArrayIndexKey: <explanation>
					<SwiperSlide key={index}>{renderItem(item, index)}</SwiperSlide>
				))}
			</Swiper>
		</>
	);
};

export default SwiperSliderPagination;
