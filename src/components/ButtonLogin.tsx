"use client";
import React, { useEffect, useRef, useState } from "react";
import { Dialog, DialogContent, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from "@/components/ui/form";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { Loader2 } from "lucide-react";
import { UserType } from "@/interfaces";
import { FormLoginType, login } from "@/lib/auth";

const formSchema = z.object({
	username: z.string().min(2, {
		message: "Username is required.",
	}),
	password: z.string().min(2, {
		message: "Password is required.",
	}),
});

const ButtonLogin = () => {
	const router = useRouter();
	const [open, setOpen] = useState(false);
	const redirectRef = useRef<NodeJS.Timeout | number>(0);

	const form = useForm<FormLoginType>({
		resolver: zodResolver(formSchema),
		defaultValues: {
			username: "",
			password: "",
		},
	});

	const loginMutation = useMutation({
		mutationFn: (formData: FormLoginType) => login(formData),
	});

	const onSubmit = (values: FormLoginType) => {
		loginMutation.mutate(values, {
			onSuccess: async (res: UserType) => {
				router.refresh();
				onClose();
				redirectRef.current = setTimeout(() => {
					router.push("/dashboard/account"); //Đăng nhập xong điều hướng sang màn quản lý tài khoản
				}, 2000);
			},
		});
	};

	const onClose = () => {
		form.reset();
		form.clearErrors();
		setOpen(false);
	};
	const onOpen = () => setOpen(true);

	useEffect(() => clearTimeout(redirectRef?.current), []);

	return (
		<>
			<Dialog open={open}>
				<DialogTrigger asChild>
					<Button variant={"link"} onClick={onOpen}>
						Đăng nhập
					</Button>
				</DialogTrigger>
				<DialogContent className="sm:max-w-[343px]" onFocusOutside={onClose} onInteractOutside={onClose}>
					<DialogHeader>
						<DialogTitle className={"title"}>Đăng nhập</DialogTitle>
					</DialogHeader>
					<Form {...form}>
						<form onSubmit={form.handleSubmit(onSubmit)} className="space-y-5">
							<FormField
								control={form.control}
								name="username"
								render={({ field }) => (
									<FormItem>
										<FormLabel>Tên tài khoản</FormLabel>
										<FormControl>
											<Input placeholder="Nhập tên tài khoản" {...field} />
										</FormControl>
										<FormMessage />
									</FormItem>
								)}
							/>
							<FormField
								control={form.control}
								name="password"
								render={({ field }) => (
									<FormItem>
										<FormLabel>Mật khẩu</FormLabel>
										<FormControl>
											<Input placeholder="Nhập mật khẩu" {...field} />
										</FormControl>
										<FormMessage />
									</FormItem>
								)}
							/>
							<DialogFooter>
								<Button variant={"outline"} className={"w-full"} onClick={onClose}>
									Đóng
								</Button>
								<Button type="submit" className={"w-full"} disabled={loginMutation.isPending}>
									{loginMutation.isPending && <Loader2 className="mr-2 h-4 w-4 animate-spin" />}
									Đăng nhập
								</Button>
							</DialogFooter>
						</form>
					</Form>
				</DialogContent>
			</Dialog>
		</>
	);
};

export default ButtonLogin;
