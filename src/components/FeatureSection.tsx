import React, { ReactNode } from "react";
import DocumentFileTitleIcon from "@/components/icons/DocumentFileTitle";
import DocumentFileCheckmarkIcon from "@/components/icons/DocumentFileCheckmark";
import DocumentFileCopyIcon from "@/components/icons/DocumentFileCopy";

type CardFeatureProps = {
	id: number;
	icon: ReactNode;
	title: ReactNode;
	description: ReactNode;
};
const CardFeature = ({ icon, title, description }: CardFeatureProps) => {
	return (
		<div className={"py-10 px-4"}>
			{icon}
			<div className="title mb-2">{title}</div>
			<p className={"text-base text-neutral-foreground"}>{description}</p>
		</div>
	);
};

const Features: CardFeatureProps[] = [
	{
		id: 1,
		icon: <DocumentFileTitleIcon className={"mb-4"} />,
		title: "100% online",
		description: "Đăng ký và nộp hồ sơ trực tuyến",
	},
	{
		id: 2,
		icon: <DocumentFileCheckmarkIcon className={"mb-4"} />,
		title: "Phê duyệt siêu tốc",
		description: "Đăng ký và nộp hồ sơ trực tuyến",
	},
	{
		id: 3,
		icon: <DocumentFileCopyIcon className={"mb-4"} />,
		title: "Sử dụng linh hoạt",
		description: "Dễ dàng chuyển đổi linh hoạt giữa các sản phẩm",
	},
];
const FeatureSection = () => {
	return (
		<div className={"relative h-[216px] max-h-fit container mx-auto"}>
			<div
				className={
					"absolute shadow-2xl grid grid-cols-4 h-full w-[calc(100%_-_4rem)] -top-1/2 z-10 bg-white rounded-2xl border border-solid border-neutral-200"
				}
			>
				<div className={"p-12"}>
					<p className={"text-2xl font-bold leading-8"}>Vì sao nên chọn chúng tôi</p>
				</div>
				{Features.map((feature) => (
					<CardFeature key={feature.id} {...feature} />
				))}
			</div>
		</div>
	);
};

export default FeatureSection;
