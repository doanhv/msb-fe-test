import React from "react";
import { Card, CardContent, CardFooter, CardHeader } from "@/components/ui/card";
import Image from "next/legacy/image";
import { Button } from "@/components/ui/button";
import { ProductType } from "@/interfaces";
import Link from "next/link";
import ArrowRightIcon from "@/components/icons/ArrowRight";

const ProductCard = ({ thumbnail, title, description, path }: ProductType) => {
	return (
		<Card className={"flex flex-col overflow-hidden group rounded-2xl transition duration-200 hover:shadow-xl"}>
			<CardHeader className={"p-0 max-h-[210px] overflow-hidden"}>
				<Image
					src={thumbnail}
					blurDataURL={thumbnail}
					alt={title}
					width={690}
					height={460}
					placeholder={"blur"}
					className={"transition duration-200 group-hover:scale-110"}
				/>
			</CardHeader>
			<CardContent className={"pt-6 pb-0 px-4 mb-4 flex-grow"}>
				<div className={"title mb-2"}>{title}</div>
				<p className={"text-neutral-foreground"}>{description}</p>
			</CardContent>
			<CardFooter className={"p-0 pb-6 px-4"}>
				<Link href={path} passHref>
					<Button className={"link"} variant={"link"}>
						Khám phá ngay
						<ArrowRightIcon className={"ml-2"} />
					</Button>
				</Link>
			</CardFooter>
		</Card>
	);
};

export default ProductCard;
