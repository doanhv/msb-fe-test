import React from "react";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import Image from "next/legacy/image";

const HeroSection = () => {
	return (
		<section className="bg-hero-corner bg-left-top bg-no-repeat relative bg-[#F3FBFD] pb-[90px]">
			<div className={"container mx-auto"}>
				<div className={"flex flex-col-reverse lg:flex-row"}>
					<div className="mr-auto place-self-center max-w-[423px] py-10 lg:py-32">
						<h1 className="mb-4">
							Trải nghiệm sống cực chất cho <br /> dân văn phòng
						</h1>
						<p className="mb-8 text-neutral-foreground">
							Lương từ 8 triệu/tháng, nhận ngay tới <br />
							200 triệu VND
						</p>
						<Link href={"/notfound"} passHref>
							<Button className={"w-[185px]"}>Khám phá ngay</Button>
						</Link>
					</div>
					<div className={"flex items-center justify-center"}>
						<Image
							src={"/hero.png"}
							blurDataURL={"/hero.png"}
							width={550}
							height={540}
							placeholder={"blur"}
							alt={"Hero banner"}
						/>
					</div>
				</div>
			</div>
		</section>
	);
};

export default HeroSection;
