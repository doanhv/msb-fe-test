import React from "react";
import { Skeleton } from "@/components/ui/skeleton";

const DropdownMenuProfileSkeleton = () => {
	return (
		<div className="flex items-center space-x-2">
			<Skeleton className="h-8 w-8 rounded-full" />
			<Skeleton className="h-5 w-[120px]" />
		</div>
	);
};

export default DropdownMenuProfileSkeleton;
