"use client";
import React from "react";
import { Card, CardContent, CardHeader } from "@/components/ui/card";
import { FullName, MenuSidebarItem } from "@/components";
import { MenuType, UserType } from "@/interfaces";
import UserIcon from "@/components/icons/User";
import FileTextIcon from "@/components/icons/FileText";
import LogoutIcon from "@/components/icons/Logout";

const SIDE_BARS: MenuType[] = [
	{
		id: "1",
		label: "Thông tin tài khoản",
		icon: <UserIcon />,
		path: "/dashboard/account",
	},
	{
		id: "2",
		label: "Thông tin sản phẩm",
		icon: <FileTextIcon />,
		path: "/dashboard/product",
	},
	{
		id: "3",
		label: "Đăng xuất",
		icon: <LogoutIcon />,
	},
];
const Sidebar = ({ user }: { user?: UserType }) => {
	return (
		<div className={"w-[333px]"}>
			<Card className={"border border-solid border-neutral-200 overflow-hidden"}>
				<CardHeader className={"title p-4 border-b border-solid border-neutral-200"}>
					<FullName user={user} />
				</CardHeader>
				<CardContent className={"p-0"}>
					<ul>
						{SIDE_BARS.map((sidebar) => (
							<MenuSidebarItem key={sidebar.id} {...sidebar} />
						))}
					</ul>
				</CardContent>
			</Card>
		</div>
	);
};

export default Sidebar;
