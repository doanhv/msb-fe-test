"use client";
import React, { Fragment, useCallback, useState } from "react";
import {
	DropdownMenu,
	DropdownMenuTrigger,
	DropdownMenuContent,
	DropdownMenuItem,
} from "@/components/ui/dropdown-menu";
import { Button } from "@/components/ui/button";
import { MenuType } from "@/interfaces";
import { DropdownMenuSubCustom } from "@/components";
import Link from "next/link";
import { ChevronRight } from "lucide-react";
import { usePathname } from "next/navigation";

interface PointerEvent<T = Element> {
	pointerId: number;
	pressure: number;
	tangentialPressure: number;
	tiltX: number;
	tiltY: number;
	twist: number;
	width: number;
	height: number;
	pointerType: "mouse" | "pen" | "touch";
	isPrimary: boolean;
}

const MENU: MenuType[] = [
	{
		id: "nav1",
		label: "Sản phẩm",
		children: [
			{
				id: "menu1",
				label: "Thẻ tín dụng",
				children: [
					{
						id: "menu1-sub1",
						label: "MSB Mastercard",
						path: "/notfound",
					},
					{
						id: "menu1-sub2",
						label: "MSB Mastercard Super Free",
						path: "/notfound",
					},
				],
			},
			{
				id: "menu2",
				label: "Vay",
				children: [
					{
						id: "menu1-sub1",
						label: "MSB Mastercard",
						path: "/notfound",
					},
					{
						id: "menu2-sub2",
						label: "MSB Mastercard Super Free",
						path: "/notfound",
					},
					{
						id: "menu3-sub3",
						label: "MSB Visa Online",
						path: "/notfound",
					},
					{
						id: "menu3-sub4",
						label: "MSB Visa Travel",
						path: "/notfound",
					},
					{
						id: "menu3-sub5",
						label: "MSB Visa Signature",
						path: "/notfound",
					},
				],
			},
			{
				id: "menu2",
				label: "Bảo hiểm",
				path: "/insurance",
			},
		],
	},
	{
		id: "nav3",
		label: "Câu hỏi thường gặp",
		path: "/questions",
	},
];
const Navigation = () => {
	const [isOpenMenu, setIsOpenMenu] = useState(false);
	const pathname = usePathname();
	const onPointerEnter = (event: PointerEvent<HTMLButtonElement>) => {
		if (event.pointerType === "mouse") {
			setIsOpenMenu(true);
		}
	};
	const onPointerLeave = (event: PointerEvent<HTMLButtonElement>) => {
		if (event.pointerType === "mouse") {
			setIsOpenMenu(false);
		}
	};

	const onCloseMenu = useCallback(() => setIsOpenMenu(false), []);

	const nav = MENU.map((nav) => {
		if (nav.children) {
			return (
				<DropdownMenu key={nav.id} open={isOpenMenu} modal={false}>
					<DropdownMenuTrigger asChild onPointerEnter={onPointerEnter} className={"focus-visible:outline-none"}>
						<Button variant={"ghost"}>
							{nav.label}
							<ChevronRight className="ml-2 h-4 w-4 rotate-90" />
						</Button>
					</DropdownMenuTrigger>
					<DropdownMenuContent onPointerLeave={onPointerLeave} sideOffset={20}>
						{nav?.children
							? nav.children.map((menu) => (
									<Fragment key={menu.id}>
										{menu.children ? (
											<DropdownMenuSubCustom menu={menu} onCloseMenu={onCloseMenu} />
										) : (
											<DropdownMenuItem
												onSelect={() => setIsOpenMenu(false)}
												asChild
												className={
													pathname.includes(menu.path as string) ? "text-primary-500 bg-primary-25" : undefined
												}
											>
												<Link href={menu.path as string}>{menu.label}</Link>
											</DropdownMenuItem>
										)}
									</Fragment>
							  ))
							: undefined}
					</DropdownMenuContent>
				</DropdownMenu>
			);
		}
		return (
			<Link href={nav.path as string} key={nav.id}>
				<Button variant={"ghost"} className={pathname.includes(nav.path as string) ? "text-primary-500" : undefined}>
					{nav.label}
				</Button>
			</Link>
		);
	});

	return nav;
};

export default Navigation;
