"use client";
import React from "react";
import Link from "next/link";
import { ChevronRight } from "lucide-react";
import { usePathname } from "next/navigation";

const transformLink = {
	account: "Thông tin tài khoản",
	product: "Thông tin sản phẩm",
};

const Breadcrumbs = () => {
	const paths = usePathname();
	const pathNames = paths.split("/").filter((path) => path);

	return (
		<nav className="flex mb-6">
			<ol className="inline-flex items-center">
				<li className="inline-flex items-center">
					<Link href={"/"}>
						<span>Trang chủ</span>
					</Link>
					{pathNames.length > 0 && <ChevronRight className="mx-1 h-4 w-4" />}
				</li>
				{pathNames.map((link, index) => {
					const href = `/${pathNames.slice(0, index + 1).join("/")}`;
					const itemClasses = paths === href ? "ms-1 text-primary-500 font-semibold" : "";

					if (index > 0) {
						return (
							<React.Fragment key={link}>
								<li className={itemClasses}>
									<Link href={href}>{transformLink[link as "account" | "product"]}</Link>
								</li>
								{pathNames.length !== index + 1 && <ChevronRight className="mx-1 h-4 w-4" />}
							</React.Fragment>
						);
					}
					return null;
				})}
			</ol>
		</nav>
	);
};

export default Breadcrumbs;
