import { ReactNode } from "react";

export type ProductType = {
	id: number;
	thumbnail: string;
	title: string;
	description?: string;
	path: string;
};
export type MenuType = {
	id: string;
	label: string;
	children?: MenuType[];
	path?: string;
	icon?: ReactNode;
};

export type UserType = {
	id: number;
	username: string;
	email: string;
	firstName?: string;
	lastName?: string;
	gender?: string;
	image?: string;
	token: string;
};

export enum ComponentType {
	input = "input",
	textarea = "textarea",
	radio = "radio",
	checkbox = "checkbox",
	select = "select",
}
