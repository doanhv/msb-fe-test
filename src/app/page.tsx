import { PageContent } from "@/components";
import { Separator } from "@/components/ui/separator";
import { NEXT_SEO_DEFAULT } from "@/app/next-seo.config";
import { Metadata } from "next";

export const metadata: Metadata = {
	...NEXT_SEO_DEFAULT,
	title: "Trang chủ",
};

export default function Home() {
	return (
		<>
			<PageContent />
			<Separator />
		</>
	);
}
