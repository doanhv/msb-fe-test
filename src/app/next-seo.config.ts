import { Metadata } from "next";

export const NEXT_SEO_DEFAULT: Metadata = {
	title: {
		default: "MSB",
		template: "%s | MSB",
	},
	icons: {
		icon: "/logo-ngang.svg",
	},
};
