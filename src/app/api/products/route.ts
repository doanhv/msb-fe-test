import { ProductType } from "@/interfaces";

const products: ProductType[] = [
	{
		id: 1,
		thumbnail: "/card-1.png",
		title: "Thẻ tín dụng",
		description: "Đa dạng lựa chọn theo phong cách chi tiêu",
		path: "/",
	},
	{
		id: 2,
		thumbnail: "/card-2.png",
		title: "Vay Linh Hoạt",
		description: "Giải ngân tức thì, tiêu dùng linh hoạt",
		path: "/",
	},
	{
		id: 3,
		thumbnail: "/card-3.png",
		title: "Mua trước trả sau",
		description: "Chuyển đổi trả góp, nhẹ ví chi tiêu",
		path: "/",
	},
	{
		id: 4,
		thumbnail: "/card-4.png",
		title: "Tiền nhanh",
		description: "Nhận khoản vay dự phòng, chủ động và nhanh chóng.",
		path: "/",
	},
	{
		id: 5,
		thumbnail: "/card-5.png",
		title: "Tài khoản thanh toán M-Pro",
		description: "Siêu miễn phí, hoàn tiền tới 3,6 triệu đồng",
		path: "/",
	},
	{
		id: 6,
		thumbnail: "/card-6.png",
		title: "Bảo hiểm ",
		description: "Mua bảo hiểm trực tuyến dễ dàng chỉ với vài thao tác",
		path: "/",
	},
];
export async function GET() {
	return Response.json(products);
}
