import { cookies } from "next/headers";
import { MSB_AUTH_COOKIE } from "@/lib/utils";
export async function POST(request: Request) {
	const nextCookies = cookies();
	nextCookies.delete(MSB_AUTH_COOKIE);
	return Response.json({});
}
