import { cookies } from "next/headers";
import { MSB_AUTH_COOKIE } from "@/lib/utils";
export async function POST(request: Request) {
	const body = await request.json();
	try {
		const res = await fetch("https://dummyjson.com/auth/login", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(body),
		});

		const data = await res.json();
		cookies().set(MSB_AUTH_COOKIE, JSON.stringify(data));
		return Response.json(data);
	} catch (e) {
		console.log(e);
	}
}
