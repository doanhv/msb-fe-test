import React from "react";
import { CardDashboard } from "@/components";
import { Metadata } from "next";
import { NEXT_SEO_DEFAULT } from "@/app/next-seo.config";

export const metadata: Metadata = {
	...NEXT_SEO_DEFAULT,
	title: "Thông tin sản phẩm",
};
const Page = () => {
	return <CardDashboard title={"Thông tin sản phẩm"} />;
};

export default Page;
