import React from "react";
import { CardDashboard, FullName } from "@/components";
import { getCookieServer } from "@/lib/getCookieServer";
import { Metadata } from "next";
import { NEXT_SEO_DEFAULT } from "@/app/next-seo.config";

export const metadata: Metadata = {
	...NEXT_SEO_DEFAULT,
	title: "Thông tin tài khoản",
};
const Page = () => {
	const user = getCookieServer();

	return (
		<CardDashboard title={"Thông tin tài khoản"}>
			<div className={"space-y-4"}>
				<div className={"flex items-center gap-4"}>
					<p className={"text-neutral-foreground w-[120px]"}>Họ và tên</p>
					<p>
						<FullName user={user} />
					</p>
				</div>
				<div className={"flex items-center gap-4"}>
					<p className={"text-neutral-foreground w-[120px]"}>Số CMND/CCCD</p>
					<p>2341234123412423</p>
				</div>
				<div className={"flex items-center gap-4"}>
					<p className={"text-neutral-foreground w-[120px]"}>Số điện thoại</p>
					<p>09893482342</p>
				</div>
				<div className={"flex items-center gap-4"}>
					<p className={"text-neutral-foreground w-[120px]"}>Email</p>
					<p>{user?.email}</p>
				</div>
			</div>
		</CardDashboard>
	);
};

export default Page;
