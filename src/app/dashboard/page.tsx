import React from "react";
import Link from "next/link";
import { ChevronRight } from "lucide-react";
import { Sidebar } from "@/components";
import { Metadata } from "next";
import { NEXT_SEO_DEFAULT } from "@/app/next-seo.config";

export const metadata: Metadata = {
	...NEXT_SEO_DEFAULT,
	title: "Dashboard",
};

const Page = () => {
	return (
		<div className={"container mx-auto h-screen py-6"}>
			<nav className="flex mb-6">
				<ol className="inline-flex items-center">
					<li className="inline-flex items-center">
						<Link href={"/"}>
							<span>Home</span>
						</Link>
					</li>
					<li>
						<div className="flex items-center">
							<ChevronRight className="mx-1 h-4 w-4" />
							<span className="ms-1 text-primary-500 font-semibold">Flowbite</span>
						</div>
					</li>
				</ol>
			</nav>
			<div className={"flex gap-6"}>
				<Sidebar />
				<div className={"flex-1"}>2</div>
			</div>
		</div>
	);
};

export default Page;
