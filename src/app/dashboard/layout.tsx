import React, { ReactNode } from "react";
import { Sidebar, Breadcrumbs } from "@/components";
import { getCookieServer } from "@/lib/getCookieServer";

const Layout = ({ children }: { children?: ReactNode }) => {
	const user = getCookieServer();

	return (
		<>
			<div className={"container mx-auto h-screen py-6"}>
				<Breadcrumbs />
				<div className={"flex gap-6"}>
					<Sidebar user={user} />
					<div className={"flex-1"}>{children}</div>
				</div>
			</div>
		</>
	);
};

export default Layout;
