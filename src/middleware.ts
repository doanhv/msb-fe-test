import { NextResponse } from "next/server";
import type { NextFetchEvent, NextRequest } from "next/server";
import { MSB_AUTH_COOKIE } from "@/lib/utils";

export const config = {
	matcher: ["/dashboard/:path*"],
};
export async function middleware(request: NextRequest, ev: NextFetchEvent) {
	const { pathname } = request.nextUrl;

	const requestHeaders = new Headers(request.headers);

	if (!request.cookies.get(MSB_AUTH_COOKIE)) {
		return NextResponse.redirect(process.env.NEXT_PUBLIC_BASE_URL as string);
	}
	return NextResponse.next({
		request: {
			headers: requestHeaders,
		},
	});
}
