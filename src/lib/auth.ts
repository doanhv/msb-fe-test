export type FormLoginType = {
	username: string;
	password: string;
};
export const login = async (values: FormLoginType) => {
	return fetch("/api/auth/login", {
		method: "POST",
		headers: { "Content-Type": "application/json" },
		body: JSON.stringify(values),
	}).then((res) => res.json());
};

export const logout = () => {
	return fetch("/api/auth/logout", {
		method: "POST",
		headers: { "Content-Type": "application/json" },
	});
};
