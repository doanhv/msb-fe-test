export const getProducts = () => fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/products`).then((res) => res.json());
