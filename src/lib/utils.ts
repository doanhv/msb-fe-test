import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
	return twMerge(clsx(inputs));
}

export const getFullName = (firstName?: string, lastName?: string) => {
	if (!firstName && !lastName) return "";
	if (!firstName) {
		return `${lastName}`.trim();
	}
	if (!lastName) {
		return `${firstName}`.trim();
	}
	return `${firstName} ${lastName}`.trim();
};

export const MSB_AUTH_COOKIE = "msb-auth";
