import { ComponentType } from "@/interfaces";

export type FormItemType = {
	label: string;
	placeholder?: string;
	name: string;
	type: ComponentType;
};
export const getFormAskForAdvices: FormItemType[][] = [
	[
		{
			label: "Họ và tên",
			placeholder: "Nhập họ và tên",
			name: "fullName",
			type: ComponentType.input,
		},
	],
	[
		{
			label: "Số điện thoại",
			placeholder: "Nhập số điện thoại",
			name: "phone",
			type: ComponentType.input,
		},
	],
	[
		{
			label: "Thành phố",
			placeholder: "Tìm thành phố...",
			name: "province",
			type: ComponentType.select,
		},
		{
			label: "Quận/Huyện",
			placeholder: "Tìm quận/huyện...",
			name: "district",
			type: ComponentType.select,
		},
	],
	[
		{
			label: "Giới tính",
			placeholder: "",
			name: "gender",
			type: ComponentType.radio,
		},
	],
	[
		{
			label: "Sản phẩm cần tư vấn",
			placeholder: "",
			name: "productAdvices",
			type: ComponentType.checkbox,
		},
	],
	[
		{
			label: "Chúng tôi có thể hỗ trợ gì cho bạn?",
			placeholder: "Nhập thông tin",
			name: "description",
			type: ComponentType.textarea,
		},
	],
];
