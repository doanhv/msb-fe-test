export const productAdvices = [
	{
		id: "credit",
		label: "Thẻ tín dụng",
	},
	{
		id: "payLater",
		label: "Mua trước trả sau",
	},
	{
		id: "flexibleLoan",
		label: "Vay linh Hoạt",
	},
	{
		id: "accountPro",
		label: "Tài khoản M-Pro",
	},
	{
		id: "fastMoney",
		label: "Tiền nhanh",
	},
] as const;
