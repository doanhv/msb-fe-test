export const getDistricts = [
	{
		value: 130201,
		label: "Hanoi",
	},
	{
		value: 130412,
		label: "Huyện Quốc Oai",
	},
	{
		value: 130558,
		label: "Hà Đông",
	},
	{
		value: 130586,
		label: "Quận Ba Đình",
	},
	{
		value: 130587,
		label: "Quận Hà Đông",
	},
	{
		value: 130595,
		label: "Sơn Tây",
	},
];
