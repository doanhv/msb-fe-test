export const getProvinces = [
	{
		label: "Hưng Yên",
		value: "66",
	},
	{
		label: "Đồng Tháp",
		value: "45",
	},
	{
		label: "Bà Rịa-Vũng Tàu",
		value: "43",
	},
	{
		label: "Thanh Hóa",
		value: "21",
	},
	{
		label: "Kon Tum",
		value: "28",
	},
	{
		label: "Điện Biên",
		value: "71",
	},
	{
		label: "Vĩnh Phúc",
		value: "70",
	},
	{
		label: "Thái Bình",
		value: "20",
	},
	{
		label: "Quảng Nam",
		value: "27",
	},
	{
		label: "Hậu Giang",
		value: "73",
	},
	{
		label: "Cà Mau",
		value: "59",
	},
	{
		label: "Hà Giang",
		value: "03",
	},
	{
		label: "Nghệ An",
		value: "22",
	},
	{
		label: "Tiền Giang",
		value: "46",
	},
	{
		label: "Cao Bằng",
		value: "04",
	},
	{
		label: "Hải Phòng",
		value: "HP",
	},
	{
		label: "Yên Bái",
		value: "06",
	},
	{
		label: "Bình Dương",
		value: "57",
	},
	{
		label: "Ninh Bình",
		value: "18",
	},
	{
		label: "Bình Thuận",
		value: "40",
	},
	{
		label: "Ninh Thuận",
		value: "36",
	},
	{
		label: "Nam Định",
		value: "67",
	},
	{
		label: "Vĩnh Long",
		value: "49",
	},
];
