import { cookies } from "next/headers";
import { UserType } from "@/interfaces";
import { MSB_AUTH_COOKIE } from "@/lib/utils";

export const getCookieServer = () => {
	const nextCookies = cookies();
	const userServer = nextCookies.get(MSB_AUTH_COOKIE)?.value;
	if (userServer) {
		return JSON.parse(userServer) as UserType;
	}
	return undefined;
};
