/** @type {import('next').NextConfig} */
const nextConfig = {
	modularizeImports: {
		'@/components': {
			transform: '@/components/{{ member }}'
		}
	},
	redirects() {
		return [
			{
				source: '/dashboard',
				destination: '/dashboard/account',
				permanent: true
			}
		]
	},
	eslint: {
		ignoreDuringBuilds: true
	},
	typescript: {
		ignoreBuildErrors: true
	}
}

module.exports = nextConfig
