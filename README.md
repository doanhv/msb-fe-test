Môi trường phát triển của dự án:
- Node version: latest(v20.9.0)
- Yarn version: latest(1.22.1)
- React version: latest(^18)

Format, lint: [Biome](https://biomejs.dev/guides/getting-started/)

UI component: 
- [Shadcn](https://ui.shadcn.com/docs/installation)
- Tailwindcss

Form: [react-hook-form](https://react-hook-form.com/get-started), validation: [Zod](https://zod.dev/?id=introduction)

Query: [react-query](https://tanstack.com/query/latest/docs/react/overview)

## Chạy dự án: Tạo file .env.local có biến môi trường là: NEXT_PUBLIC_BASE_URL=http://localhost:9696

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Mở [http://localhost:9696](http://localhost:9696) bằng trình duyệt của anh/chị để xem kết quả

Ngoài ra dự án đã được public source code trên [gitlab](https://gitlab.com/doanhv/msb-fe-test) và đã deploy trên vercel nên anh/chị có thể preview qua [domain này](https://msb-fe-test.vercel.app
)


### Dự án sử dụng [Nextjs](https://nextjs.org/docs) app router
Trong khoảng thời gian có hạn, nên đôi khi có những chỗ chưa thể tối ưu và bao quát hết được, mong anh/chị review qua, đánh giá và nhận xét giúp em ạ. Em xin cảm ơn!
